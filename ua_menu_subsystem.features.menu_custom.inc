<?php
/**
 * @file
 * ua_menu_subsystem.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function ua_menu_subsystem_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: main-menu.
  $menus['main-menu'] = array(
    'menu_name' => 'main-menu',
    'title' => 'Main menu',
    'description' => 'The <em>Main</em> menu is used on many sites to show the major sections of the site, often in a top navigation bar.',
  );
  // Exported menu: menu-utility-links.
  $menus['menu-utility-links'] = array(
    'menu_name' => 'menu-utility-links',
    'title' => 'Utility Links',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Main menu');
  t('The <em>Main</em> menu is used on many sites to show the major sections of the site, often in a top navigation bar.');
  t('Utility Links');


  return $menus;
}
