<?php
/**
 * @file
 * ua_menu_subsystem.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ua_menu_subsystem_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
